#!/bin/sh

#####################################
########SET THESE VARIABLES##########
#####################################
export INSTANCE_HOSTNAME="baa228a6e0590b3c.csgo.prhy.me"
export SERVER_HOSTNAME=""
export STEAM_ACCOUNT=""
export SRCDS_EXTRA_ARGUMENTS=""
#####################################
########SET THESE VARIABLES##########
#####################################

if [ ! -z "$INSTANCE_HOSTNAME" ]
then
        hostnamectl set-hostname $INSTANCE_HOSTNAME
fi


if [ ! -z "$SERVER_HOSTNAME" ]
then
        echo "Setting SERVER_HOSTNAME=$SERVER_HOSTNAME"
        sed -i "/SERVER_HOSTNAME=/c\SERVER_HOSTNAME=$SERVER_HOSTNAME" /valve/csgo/docker/env/gamers4ever.csgo.env
fi


if [ ! -z "$STEAM_ACCOUNT" ]
then
        echo "Setting STEAM_ACCOUNT=$STEAM_ACCOUNT"
        sed -i "/STEAM_ACCOUNT=/c\STEAM_ACCOUNT=$STEAM_ACCOUNT" /valve/csgo/docker/env/gamers4ever.csgo.env
fi


if [ ! -z "$SRCDS_EXTRA_ARGUMENTS" ]
then
        echo "Setting SRCDS_EXTRA_ARGUMENTS=$SRCDS_EXTRA_ARGUMENTS"
        sed -i "/SRCDS_EXTRA_ARGUMENTS=/c\SRCDS_EXTRA_ARGUMENTS=$SRCDS_EXTRA_ARGUMENTS" /valve/csgo/docker/env/gamers4ever.csgo.env
fi
